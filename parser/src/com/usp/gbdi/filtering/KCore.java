package com.usp.gbdi.filtering;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Calendar;

public class KCore implements GraphProcess {

	private int step;

	/**
	 * check the max vertex to create yearsByVertex structure in the step 2
	 */
	private int maxVertex;

	/**
	 * Number of relationships per interval
	 */
	private int k;

	/**
	 * Intervals to analyze
	 */
	private Interval[] intervals;

	private Interval selectedInterval;

	boolean vertexState[];
	short relations[][];

	BufferedWriter writer;

	GraphProcessor processor;

	public KCore() {
		processor = new GraphProcessor();
	}

	public void filterGraphByK(File in, File out, Interval selectedInterval,
			int k, Interval... intervals) throws NumberFormatException,
			IOException {
		this.selectedInterval = selectedInterval;
		this.intervals = intervals;
		this.k = k;
		processor.setGraph(in);

		// defining vertex number on the in the interval
		preprocess();

		vertexState = new boolean[maxVertex + 1];
		relations = new short[maxVertex + 1][2];

		processIntervals();

		filterGraph(out);
	}

	private void preprocess() throws NumberFormatException, IOException {
		step = 1;
		maxVertex = Integer.MIN_VALUE;
		processor.run(this);

	}

	private void processIntervals() throws NumberFormatException, IOException {
		step = 2;
		processor.run(this);

		// checking the vertexes that have in all intervals k relations

		int n = maxVertex + 1;
		int intervals = this.intervals.length;
		boolean state = false;

		for (int i = 0; i < n; i++) {
			state = true;
			for (int j = 0; j < intervals; j++) {
				if (relations[i][j] / 2 < k) { // divide by 2 because the graph
												// is undirected
					state = false;
					break;
				}
			}
			vertexState[i] = state;
		}

	}

	private void filterGraph(File out) throws NumberFormatException,
			IOException {
		step = 3;
		writer = new BufferedWriter(new FileWriter(out));
		processor.run(this);
		writer.flush();
		writer.close();
	}

	@Override
	public void iter(int inVertex, int outVertex, float weight,
			Calendar timestamp) {
		int year = timestamp.get(Calendar.YEAR);

		switch (step) {
		case 1:
			maxVertex = Math.max(Math.max(inVertex, outVertex), maxVertex);
			break;
		case 2:
			for (int i = 0; i < intervals.length; i++) {
				if (intervals[i].contains(year)) {
					relations[inVertex][i]++;
					relations[outVertex][i]++;
				}
			}
			break;
		case 3:
			boolean validYear = false;
			for (Interval interval : intervals) {
				if (interval.contains(year)) {
					validYear = true;
					break;
				}
			}
			// where is selected
			if (selectedInterval != null && !selectedInterval.contains(year)) {
				validYear = false;
			}

			if (vertexState[inVertex] && vertexState[outVertex] && validYear) {
				try {
					writer.write(String.format("%d\t%d\n", inVertex, outVertex));
				} catch (IOException e) {
					e.printStackTrace();
				}
			}
			break;
		}

	}

	public static void main(String args[]) {
		try {
	
			try {
				// int k = 4;
				// int interval = 1;
				// int type = 1; // 1 = all, 2 = train, 3=test

				File in = new File("/run/media/hugo/hugo-disk/hugo/disciplinas/redes_complexas/workspace/dblp/src/out.dblp_coauthor");
				File out_directory = new File("/home/hugo/Downloads/destination_directory");
				
				out_directory.mkdirs();
				
				for (int k = 4; k <= 6; k++) {
					for (int interval = 1; interval <= 4; interval++) {
						for (int type = 1; type <= 3; type++) {

							String stype = (type == 1 ? "full"
									: type == 2 ? "train" : "test");

							
							File out = new File(out_directory, 
											k + "-core/" + stype + "/interval"
											+ interval);
							
							out.getParentFile().mkdirs();
							KCore kcore = new KCore();

							/*
							 * COMPLETE INTERVALS
							 */

							// interval 1
							// kcore.filterGraphByK(in, out, null, k, new
							// Interval(1974, 2012) );

							// interval 2
							// kcore.filterGraphByK(in, out, null, k, new
							// Interval(1995, 2007) );

							// interval 3
							// kcore.filterGraphByK(in, out, null, k, new
							// Interval(1990, 2004) );

							// interval 4
							// kcore.filterGraphByK(in, out, null, k, new
							// Interval(1995, 2004) );

							/*
							 * COMPLETE INTERVALS REDUCING TO SUBINTERVAL TRAIN
							 */

							// interval 1
							// kcore.filterGraphByK(in, out, new Interval(1974,
							// 2009), k, new Interval(1974, 2012) );

							// interval 2
							// kcore.filterGraphByK(in, out, new Interval(1995,
							// 2005), k, new Interval(1995, 2007) );

							// interval 3
							// kcore.filterGraphByK(in, out, new Interval(1990,
							// 1999), k, new Interval(1990, 2004) );

							// interval 4
							// kcore.filterGraphByK(in, out, new Interval(1995,
							// 1999), k, new Interval(1995, 2004) );

							/*
							 * COMPLETE INTERVALS REDUCING TO SUBINTERVAL TEST
							 */

							// interval 1
							// kcore.filterGraphByK(in, out, new Interval(2010,
							// 2012), k, new Interval(1974, 2012) );

							// interval 2
							// kcore.filterGraphByK(in, out, new Interval(2006,
							// 2007), k, new Interval(1995, 2007) );

							// interval 3
							// kcore.filterGraphByK(in, out, new Interval(2000,
							// 2004), k, new Interval(1990, 2004) );

							// interval 4
							// kcore.filterGraphByK(in, out, new Interval(2000,
							// 2004) , k, new Interval(1995, 2004) );

							/*
							 * K-CORE COMPLETE INTERVAL
							 */

							if (type == 1) {
								switch (interval) {
								case 1:
									kcore.filterGraphByK(in, out, null, k,
											new Interval(1974, 2009), new Interval(
													2010, 2012));
									break;
								case 2:
									kcore.filterGraphByK(in, out, null, k,
											new Interval(1995, 2005), new Interval(
													2006, 2007));
									break;
								case 3:
									kcore.filterGraphByK(in, out, null, k,
											new Interval(1990, 1999), new Interval(
													2000, 2004));
									break;
								case 4:
									kcore.filterGraphByK(in, out, null, k,
											new Interval(1995, 1999), new Interval(
													2000, 2004));
									break;
								}
								
							}
							/*
							 * K-CORE INTERVALS TO TRAIN
							 */

							if (type == 2) {
								switch (interval) {
								case 1:
									kcore.filterGraphByK(in, out, new Interval(
											1974, 2009), k,
											new Interval(1974, 2009), new Interval(
													2010, 2012));
									break;
								case 2:
									kcore.filterGraphByK(in, out, new Interval(
											1995, 2005), k,
											new Interval(1995, 2005), new Interval(
													2006, 2007));
									break;
								case 3:
									kcore.filterGraphByK(in, out, new Interval(
											1990, 1999), k,
											new Interval(1990, 1999), new Interval(
													2000, 2004));
									break;
								case 4:
									kcore.filterGraphByK(in, out, new Interval(
											1995, 1999), k,
											new Interval(1995, 1999), new Interval(
													2000, 2004));
									break;
								}
								
							}

							/*
							 * K-CORE INTERVALS TO TEST
							 */

							if (type == 3) {
								switch (interval) {
								case 1:
									kcore.filterGraphByK(in, out, new Interval(
											2010, 2012), k,
											new Interval(1974, 2009), new Interval(
													2010, 2012));
									break;
								case 2:
									kcore.filterGraphByK(in, out, new Interval(
											2006, 2007), k,
											new Interval(1995, 2005), new Interval(
													2006, 2007));
									break;
								case 3:
									kcore.filterGraphByK(in, out, new Interval(
											2000, 2004), k,
											new Interval(1990, 1999), new Interval(
													2000, 2004));
									break;
								case 4:
									kcore.filterGraphByK(in, out, new Interval(
											2000, 2004), k,
											new Interval(1995, 1999), new Interval(
													2000, 2004));
									break;
								}
							}
						}
					}
				}
			} catch (Exception e) {
				e.printStackTrace();
			}


					
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
