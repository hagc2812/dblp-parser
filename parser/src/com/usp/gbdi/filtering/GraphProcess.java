package com.usp.gbdi.filtering;

import java.util.Calendar;

interface GraphProcess{
	public void iter(int inVertex, int outVertex, float weight, Calendar timestamp);
}