package com.usp.gbdi.filtering;

class Interval{
	int initValue;
	int endValue;
	
	public Interval(int initValue, int endValue) {
		super();
		this.initValue = initValue;
		this.endValue = endValue;
	}
	
	public boolean contains(int value){
		if(value >= initValue && value <=endValue){
			return true;
		}
		return false;
	}
	
}