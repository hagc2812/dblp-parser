package com.usp.gbdi.filtering;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Calendar;

public class GraphProcessor {
		
	public void setGraph(File graph){
		this.graph = graph;
	}
	
	public File getGraph(){
		return graph;
	}
	
	public void run(GraphProcess process) throws NumberFormatException, IOException{ 
		BufferedReader reader 	= null;
		String line 			= "";
		Calendar timestamp 		= Calendar.getInstance();
		String[] sections;
		int inVertex, outVertex;
		float weight;
		Calendar ref			= null;
		try{
			reader = new BufferedReader(new FileReader(graph));
			while(reader.ready()){
				line = reader.readLine();
				
				//omitting comments
				if(line.startsWith("%")){
					continue;
				}
				sections 	= line.split(" ");
				if(sections.length == 1){
					sections 	= line.split("\t");
				}
				inVertex 	= Integer.parseInt(sections[0]);
				outVertex 	= Integer.parseInt(sections[1]);
				weight 		= sections.length >2? Float.parseFloat(sections[2]): 1;
				
				//change the time in seconds to milliseconds because in java is required
				ref = timestamp;
				if(sections.length > 3){
					ref.setTimeInMillis(Long.parseLong(sections[3]) * 1000);
				}else{
					ref = null;
				}
					
				process.iter(inVertex, outVertex, weight, ref);
			}
			reader.close();
		}
		catch(IOException e){
			if(reader != null){
				reader.close();
			}
			throw e;
		}
		
	}
	
	File graph;
}



